/*
 *  Copyright 2015 Adobe Systems Incorporated
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.bi.aem.yammer.demo.core.models;

import javax.inject.Inject;
import javax.inject.Named;
import javax.jcr.Node;
import javax.jcr.RepositoryException;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.commons.inherit.HierarchyNodeInheritanceValueMap;
import com.day.cq.commons.inherit.InheritanceValueMap;
import com.day.cq.wcm.api.Page;

@Model(adaptables=Resource.class)
public class Yammer {
	private static final Logger LOGGER = LoggerFactory.getLogger(Yammer.class);
	
	private static final String SLASH_CHAR = "/";
	private static final String UNDERSCORE_CHAR = "_";
	private static final String COLON_CHAR = ":";
	private static final String PROP_NETWORK = "network";
	
    @Self
    protected Resource self;
    
    @Inject
	private Page currentPage;

    @Inject
	@Optional
    private String feedId;
    
    @Inject
	@Optional
    private String defaultGroupId;
    
    private String identifier;
    
    private String network;
    
    @Inject
	@Optional
	@Named("use_sso")
    private String useSSO;
    
    @Inject
	@Optional
    private String header;
    
    @Inject
	@Optional
    private String footer;
    
    @Inject
	@Optional
    private String hideNetworkName;
    
    @Inject
	@Optional
    private String showOpenGraphPreview;
    
    @Inject
	@Optional
    private String defaultToCanonical;
    
    
    public String getIdentifier() {
	    	if(StringUtils.isNotEmpty(identifier)) {
	    		return identifier;
	    	}
	    	
	    	try {
				identifier = self.adaptTo(Node.class).getIdentifier();
				identifier = identifier.replaceAll(SLASH_CHAR, UNDERSCORE_CHAR);
				identifier = identifier.replaceAll(COLON_CHAR, UNDERSCORE_CHAR);
			} catch (RepositoryException e) {
				LOGGER.error("Cannot adapt to Node", e);
				identifier = StringUtils.EMPTY;
			}
	    	
	    	return identifier;
    }

	public String getFeedId() {
		return feedId;
	}

	public String getDefaultGroupId() {
		return defaultGroupId;
	}
	
	public String getNetwork() {
		if(StringUtils.isNotEmpty(network)) {
    		return network;
    	}
		
		InheritanceValueMap map = new HierarchyNodeInheritanceValueMap(currentPage.getContentResource());
		network = map.getInherited(PROP_NETWORK, StringUtils.EMPTY);
		return network;
	}

	public boolean getUseSSO() {
		return "true".equals(useSSO);
	}

	public boolean getHeader() {
		return "true".equals(header);
	}

	public boolean getFooter() {
		return "true".equals(footer);
	}

	public boolean getHideNetworkName() {
		return "true".equals(hideNetworkName);
	}

	public boolean getShowOpenGraphPreview() {
		return "true".equals(showOpenGraphPreview);
	}

	public boolean getDefaultToCanonical() {
		return "true".equals(defaultToCanonical);
	}
	
	
}
